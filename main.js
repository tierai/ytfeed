
/*
TODO: Update header when .player is visible..
TODO: 403 for subs

subs: (requires share subs in channel settings)
#$sub:userId
https://gdata.youtube.com/feeds/api/users/userId/subscriptions?v=2
https://gdata.youtube.com/feeds/api/users/userId/newsubscriptionvideos

*/

function escapeHTML(text) {
    if(typeof text == 'string') {
        return text.replace(/[&<>"'`]/g, function(c){
           return '&#' + c.charCodeAt(0) + ';';
        })
    }
    return text;
}

(function($){
	$.fn.tpl = function(data){
        var text = $(this).text();
        text = text.replace(/[\$|!][\w|{][\w._]*[\w|}]/g, function(match, p1, offset, string){
            var keys = match.replace(/[^\w.]+/g, '').split('.');
            var root = data;
            for(var i = 0; i < keys.length; ++i) {
                if(typeof root[keys[i]] == 'undefined') return escapeHTML(match);
                root = root[keys[i]];
            }
            return match[0] == '$' ? escapeHTML(root) : root;
        });
        return $(text);
    }
})(jQuery);

var YTFeed = {
    defaults: {
        timer: 10,
        limit: 20,
        embed: ['live'],
        youtube: [],
        twitch: [],
    },
    options: {},

    insert: function(element) {
        var data = element.data('data');
        var test = $('#video-' + data.id);
        if(test.length) {
            test.replaceWith(element);
            return;
        }
        var items = $('#feed li');
        for(var i = 0; i < items.length; ++i) {
            if($(items[i]).data('data').index < data.index) {
                element.insertBefore(items[i]);
                return;
            }
        }
        element.appendTo('#feed');
    },

    formatDescription: function(text) {
        text.replace(/\n/g, '<br>');
        var re = /(^|\s)((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/gi;
        text = text.replace(re, function(match){
            return '<a href="' + match + '">' + match + '</a>';
        });
        return text;
    },

    getFeed: function(url) {
        if(console && console.log) console.debug(url);
        return $.ajax({
           url: url,
           dataType: 'json',
        }).always(function(result){
            if(console && console.log) console.debug(result);
        });
    },

    getYoutubeUrl: function(id) {
        var q = 'v=2&alt=jsonc&max-results=' + this.options.limit;
        if(id.indexOf(':') > -1) {
            var parts = id.split(':');
            switch(parts[0]) {
            case 'sub':
                return 'https://gdata.youtube.com/feeds/api/users/' + escape(parts[1]) + '/newsubscriptionvideos?' + q;
            }
            if(console && console.log) console.debug('Ignoring special id ' + id);
            return false;
        }
        return 'https://gdata.youtube.com/feeds/api/users/' + escape(id) + '/uploads?' + q;
    },

    getYoutubeFeed: function(id) {
        var self = this;
        var url = this.getYoutubeUrl(id);
        if(!url) return;
        this.getFeed(url).done(function(result){
            for(var i in result.data.items) {
                var item = result.data.items[i];
                if($('#video-' + item.id + ' .player').length) continue;
                item.index = new Date(item.uploaded);
                item.date = item.index.toString();
                item.description = self.formatDescription(item.description);
                item.url = 'http://www.youtube.com/watch?v=' + escape(item.id);
                item.reddit = typeof self.options.reddit[item.uploader] != 'undefined' ? self.options.reddit[item.uploader] : 'videos';
                var elem = $('#item-template').tpl(item);
                elem.data({
                    data: item,
                });
                self.insert(elem);
            }
        });
    },

    getTwitchFeed: function(id) {
        if($('#live-' + id + ' .player').length) return;
        var url = 'http://api.justin.tv/api/stream/list.json?channel=' + escape(id) + '&jsonp=?';
        this.getFeed(url).always(function(result){
            $('#live-' + id).remove();
        }).done(function(result){
            $('#live-template').tpl(result[0]).data('data', result[0]).appendTo('#live');
        });
    },

    refresh: function() {
        $('.error').remove();
        for(var i in this.options.youtube) this.getYoutubeFeed(this.options.youtube[i]);
        for(var i in this.options.twitch) this.getTwitchFeed(this.options.twitch[i]);
        if(this.options.timer) setTimeout(this.refresh, this.options.timer * 60000);
    },

    filter: function() {
        var filter = window.location.hash.substring(1);
        if(filter.length) {
            filter = filter.split('|');
            this.options.youtube = filter.indexOf('$all') > -1 ? this.defaults.youtube.concat(filter) : filter;
            this.options.youtube = this.options.youtube.filter(function(e,i,a){ return e && e[0] != '$'; });
            this.options.youtube = $.unique(this.options.youtube);
        } else {
            this.options.youtube = this.defaults.youtube;
        }
    },

    clear: function() {
        $('#feed li').remove();
    },

    init: function(options) {
        var self = this;
        if(console && console.log) console.debug(options);
        this.defaults = $.extend(this.defaults, options);
        this.options = $.extend({}, this.defaults);

        if(this.options.embed) {
            var embed = function(){
                var item = $(this).closest('li');
                var data = item.data('data');
                var content = $('.content', item);
                if(content.is(':hidden')) {
                    if($('.player', item).length < 1) {
                        if(typeof data.channel == 'undefined' || typeof data.channel.embed_code == 'undefined') {
                            var src = 'http://www.youtube.com/embed/' + data.id;
                            content.prepend('<iframe class="player" width="640" height="360" src="' + src + '" frameborder="0" allowfullscreen></iframe>');
                        } else {
                            content.prepend('<div class="player">' + data.channel.embed_code + '</div>');
                        }
                    }
                    $('li[id!=' + item.attr('id') + '] .content').hide();
                    $('li[id!=' + item.attr('id') + '] .player').remove();
                    var margin = 5;
                    var width = $(item).width() - margin * 2;
                    $('.player', item).css({
                        width: width,
                        height: width * 9 / 16,
                        marginLeft: margin,
                        marginRight: margin,
                    });
                } else {
                    $('.player', content).remove();
                }
                content.toggle();
                return false;
            };
            for(var i in this.options.embed) {
                $('#' + this.options.embed[i] + ' .link').live('click', embed);
            }
        }

        $(window).bind('hashchange', function(){
            self.clear();
            self.filter();
            self.refresh();
        });

        $('.message').live('click', function(){
            $(this).hide('fast');
        });

        $('a[href^="http://www.reddit.com"]').live('click', function(){
            var self = $(this);
            var item = self.closest('li');
            var data = item.data('data');
            var text = self.text();
            var url = 'http://www.reddit.com/r/' + escape(data.reddit) + '/submit?url=' + escape(data.url);
            self.text('Searching...');
            $.ajax({
                url: 'http://www.reddit.com/api/info.json?url=' + escape(data.url) + '&jsonp=?',
                dataType: 'json',
            }).success(function(result){
                if(result.data.children.length > 0) {
                    url = 'http://www.reddit.com' + result.data.children[0].data.permalink;
                }
            }).complete(function(){
                self.text(text);
                self.attr('href', url);
                window.open(url);
            });
            return false;
        });

        $(document).ajaxStart(function(e, xhr, settings){
            $('.loading').fadeIn();
        });
        $(document).ajaxStop(function(){
            $('.loading').fadeOut();
        });
        $(document).ajaxError(function(e, xhr, settings){
            console.debug(xhr, settings);
            var msg = 'Could not load feed, check your internet connection!<br />' + escapeHTML(settings.url);
            if(settings.url.indexOf('newsubscriptionvideos') > -1) {
                msg += '<br /><br />Trying to load your subscriptions feed?<br />Go to ';
                msg += '<a href="https://www.youtube.com/channel_editor?action_editor=1&editor_tab=branding"><u>Channel settings</u></a>';
                msg += ' &gt; <u>Tabs</u> &gt; check <u>Subscribe to a channel</u><br />Warning: This will make your subscriptions avalable to <u>anyone</u>!';

            }
            $('#error-template').tpl({
                message: msg,
            }).prependTo('#info');
        });

        self.clear();
        self.filter();
        self.refresh();
    },
};

