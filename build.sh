#!/bin/bash
D=public_html
mkdir $D
for x in *.{js,css}; do
    yui-compressor -o "$D/$x" "$x"
done
cp *.html "$D"

